//
//  DataSource.swift
//  HackingWithSwift
//
//  Created by Álvaro Ferrández Gómez on 24/06/2020.
//  Copyright © 2020 Álvaro Ferrández Gómez. All rights reserved.
//

import Foundation

public enum projects: String, CaseIterable {
    case project1 = "Storm Viewer"
    case project2 = "Guess the flag"
    case project3 = "Social Media"
    case project4 = "Easy Browser"
    case project5 = "Word Scramble"
    case project6 = "Auto Layout"
    case project7 = "Whitehouse Petitions"
    case project8 = "8" //"7 Swifty Words"
    case project9 = "9" //"Grand Central Dispatch"
    case project10 = "10" //"Names to Faces"
    case project11 = "11" //"Pachinko"
    case project12 = "12" //UserDefaults
    case project13 = "13" //"Instafilter"
    case project14 = "14" //"Whack-a-Penguin"
    case project15 = "15" //"Animation"
    case project16 = "16" //"Capital Cities"
    case project17 = "17" //"Space Race"
    case project18 = "18" //"Debugging"
    case project19 = "19" //"JavaScript Injection"
    case project20 = "20" //"Fireworks Night"
    case project21 = "21" //"Local Notifications"
    case project22 = "22" //"Detect-a-Beacon"
    case project23 = "23" //"Swifty Ninja"
    case project24 = "24" //"Swift Strings"
    case project25 = "25" //"Selfie Share"
    case project26 = "26" //"Marble Maze"
    case project27 = "27" //"Core Graphics"
    case project28 = "28" //"Secret Swift"
    case project29 = "29" //"Exploding Monkeys"
    case project30 = "30" //"Instruments"
    case project31 = "31" //"Multibrowser"
    case project32 = "32" //"SwiftSearcher"
    case project33 = "33" //"What's that Whistle?"
    case project34 = "34" //"Four in a Row"
    case project35 = "35" //"Generating random numbers"
    case project36 = "36" //"Crashy Plane"
    case project37 = "37" //Psychic Tester"
    case project38 = "38" //"GitHub Commits"
    case project39 = "39" //"Using testing with XCTest"

    static let allValues = [project1, project2, project3, project4, project5, project6, project7, project8, project9, project10, project11, project12, project13, project14, project15, project16, project17, project18, project19, project20, project21, project22, project23, project24, project25, project26, project27, project28, project29, project30, project31, project32, project33, project34, project35, project36, project37, project38, project39]
}
