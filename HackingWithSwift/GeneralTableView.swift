//
//  GeneralTableView.swift
//  HackingWithSwift
//
//  Created by Álvaro Ferrández Gómez on 23/06/2020.
//  Copyright © 2020 Álvaro Ferrández Gómez. All rights reserved.
//

import UIKit

class GeneralTableView: UIViewController {

    @IBOutlet weak var generalTableView: UITableView!

    typealias Projects = (project: Int, name: String)
    var collectionProjects = [Int: Projects]()
    var projectsArray: [Projects] = []

    override func viewDidLoad() {
        super.viewDidLoad()

        self.navigationItem.title = "Hacking With Swift"
        self.navigationController?.navigationBar.prefersLargeTitles = true

        self.getData()
        self.registerCell()
        self.setDelegate()
    }

    func registerCell() {
        generalTableView.register(UINib(nibName: "GeneralCell", bundle: nil), forCellReuseIdentifier: "GeneralCellID")
    }

    func setDelegate() {
        generalTableView.delegate = self
        generalTableView.dataSource = self
    }

    func getData() {
        for (i, project) in projects.allValues.enumerated() {
            if project.rawValue.count > 2 {
                self.projectsArray.append(Projects(project: i + 1, name: project.rawValue))
            }
        }
    }

    func getProjectData(_ indexPathRow: Int) -> (storyboard: String, viewController: String) {
        let storyboard: String = "Project\(projectsArray[indexPathRow].project)"
        let viewController: String = "GeneralProject\(projectsArray[indexPathRow].project)ViewController"
        return (storyboard, viewController)
    }
}

extension GeneralTableView: UITableViewDelegate, UITableViewDataSource {
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }

    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return projectsArray.count
    }

    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        if let cell = tableView.dequeueReusableCell(withIdentifier: "GeneralCellID") as? GeneralCell {
            cell.labelTitle.text = "Proyecto \(projectsArray[indexPath.row].project): \(projectsArray[indexPath.row].name)"
            cell.selectionStyle = .none
            return cell
        }
        return UITableViewCell()
    }

    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        let storyboardString = getProjectData(indexPath.row).storyboard
        let viewControllerString = getProjectData(indexPath.row).viewController
        let vc = UIStoryboard(name: storyboardString, bundle: nil).instantiateViewController(withIdentifier: viewControllerString)
        self.navigationController?.pushViewController(vc, animated: true)
    }
}

extension UIViewController {
    var className: String {
        return NSStringFromClass(self.classForCoder).components(separatedBy: ".").last!;
    }
}
