//
//  GeneralCell.swift
//  HackingWithSwift
//
//  Created by Álvaro Ferrández Gómez on 23/06/2020.
//  Copyright © 2020 Álvaro Ferrández Gómez. All rights reserved.
//

import UIKit

class GeneralCell: UITableViewCell {
    @IBOutlet weak var labelTitle: UILabel!
}
