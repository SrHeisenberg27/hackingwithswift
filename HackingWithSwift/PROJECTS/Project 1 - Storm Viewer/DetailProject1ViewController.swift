//
//  DetailProject1ViewController.swift
//  HackingWithSwift
//
//  Created by Álvaro Ferrández Gómez on 24/06/2020.
//  Copyright © 2020 Álvaro Ferrández Gómez. All rights reserved.
//

import UIKit

class DetailProject1ViewController: UIViewController {

    @IBOutlet weak var images: UIImageView!

    var image: String?
    var numberOfPictures: Int?
    var actualPicture: Int?

    override func viewDidLoad() {
        super.viewDidLoad()

        if let image = self.image, let actualPicture = self.actualPicture, let numberOfPictures = self.numberOfPictures {
            self.navigationItem.title = image + " - " + String(actualPicture) + "/" + String(numberOfPictures)
            self.navigationItem.largeTitleDisplayMode = .never
            self.images.image = UIImage(named: image)
        }
    }

    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        navigationController?.hidesBarsOnTap = true
    }

    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
        navigationController?.hidesBarsOnTap = false
    }
}
