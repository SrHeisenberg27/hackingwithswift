//
//  GeneralProject1ViewController.swift
//  HackingWithSwift
//
//  Created by Álvaro Ferrández Gómez on 23/06/2020.
//  Copyright © 2020 Álvaro Ferrández Gómez. All rights reserved.
//

import UIKit

class GeneralProject1ViewController: UIViewController {

    @IBOutlet weak var tableViewProject1: UITableView!

    var titleNavBar: String = "Proyecto 1: Storm Viewer"
    var pictures: [String] = []
    var orderPictures: [String] = []
    var numberOfPictures: Int = 0
    var items: [String] = []

    override func viewDidLoad() {
        super.viewDidLoad()

        self.navigationItem.title = titleNavBar
        self.navigationItem.largeTitleDisplayMode = .never

        let fm = FileManager.default
        let path = Bundle.main.resourcePath

        if let path = path {
            guard let items = try? fm.contentsOfDirectory(atPath: path) else { return }
            self.items = items
        }

        for item in self.items {
            if item.hasPrefix("nssl") {
                // this is a picture to load
                self.pictures.append(item)
            }
        }

        print(self.pictures)

        self.orderPictures = self.pictures.sorted()
        self.numberOfPictures = self.orderPictures.count

        tableViewProject1.delegate = self
        tableViewProject1.dataSource = self
    }
}

extension GeneralProject1ViewController: UITableViewDelegate, UITableViewDataSource {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return self.orderPictures.count
    }

    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableViewProject1.dequeueReusableCell(withIdentifier: "PictureCellID", for: indexPath)
        cell.textLabel?.text = self.orderPictures[indexPath.row]
        return cell
    }

    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        let storyboard = UIStoryboard(name: "Project1", bundle: nil)
        if let viewController = storyboard.instantiateViewController(withIdentifier: "DetailProject1ViewController") as? DetailProject1ViewController {
            viewController.image = self.orderPictures[indexPath.row]
            viewController.numberOfPictures = self.numberOfPictures
            viewController.actualPicture = indexPath.row + 1
            self.navigationController?.pushViewController(viewController, animated: true)
        }
    }
}




