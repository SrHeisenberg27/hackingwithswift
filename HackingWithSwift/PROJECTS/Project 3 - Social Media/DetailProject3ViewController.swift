//
//  DetailProject3ViewController.swift
//  HackingWithSwift
//
//  Created by Álvaro Ferrández Gómez on 24/06/2020.
//  Copyright © 2020 Álvaro Ferrández Gómez. All rights reserved.
//

import UIKit

class DetailProject3ViewController: UIViewController {

    @IBOutlet weak var images: UIImageView!

    var image: String?
    var numberOfPictures: Int?
    var actualPicture: Int?

    override func viewDidLoad() {
        super.viewDidLoad()

        if let image = self.image, let actualPicture = self.actualPicture, let numberOfPictures = self.numberOfPictures {
            self.navigationItem.title = image + " - " + String(actualPicture) + "/" + String(numberOfPictures)
            self.navigationItem.largeTitleDisplayMode = .never
            self.navigationItem.rightBarButtonItem = UIBarButtonItem(barButtonSystemItem: .action, target: self, action: #selector(shareTapped))
            self.images.image = UIImage(named: image)
        }
    }

    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        navigationController?.hidesBarsOnTap = true
    }

    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
        navigationController?.hidesBarsOnTap = false
    }

    @objc func shareTapped() {

        /*
        IMPORTANTE:

         PARA GUARDARLO EN LA GALERIA NECESITAMOS DAR PERMISOS DE PRIVACIDAD EN EL INFO.PLIST, photo libray addition
         
         */

        guard let image = images.image?.jpegData(compressionQuality: 0.8) else { return }
        guard let imageName = self.image else { return }

        let vc = UIActivityViewController(activityItems: [image, imageName.uppercased()], applicationActivities: [])
        vc.popoverPresentationController?.barButtonItem = navigationItem.rightBarButtonItem
        present(vc, animated: true)
    }
}
