//
//  GeneralProject7bViewController.swift
//  HackingWithSwift
//
//  Created by Álvaro Ferrández Gómez on 25/06/2020.
//  Copyright © 2020 Álvaro Ferrández Gómez. All rights reserved.
//

import UIKit

class GeneralProject7bViewController: UITabBarController {

    @IBOutlet weak var tableView: UITableView!

    var petitions = ["a", "b"]

    override func viewDidLoad() {
        super.viewDidLoad()

        self.navigationItem.largeTitleDisplayMode = .never

        tableView.delegate = self
        tableView.dataSource = self
    }
}

extension GeneralProject7bViewController: UITableViewDataSource, UITableViewDelegate {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return petitions.count
    }

    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "CellID", for: indexPath)
        cell.textLabel?.text = "Title eeeeeeee here"
        cell.detailTextLabel?.text = "Subtitle goes here"
        return cell
    }
}
