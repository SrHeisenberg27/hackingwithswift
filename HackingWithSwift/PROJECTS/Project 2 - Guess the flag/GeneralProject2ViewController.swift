//
//  GeneralProject2ViewController.swift
//  HackingWithSwift
//
//  Created by Álvaro Ferrández Gómez on 23/06/2020.
//  Copyright © 2020 Álvaro Ferrández Gómez. All rights reserved.
//

import UIKit

class GeneralProject2ViewController: UIViewController {

    @IBOutlet weak var button1: UIButton!
    @IBOutlet weak var button2: UIButton!
    @IBOutlet weak var button3: UIButton!

    var countries = [String]()
    var score: Int = 0
    var correctAnswer: Int = 0
    var currentQuestions: Int = 0
    let totalQuestions: Int = 10
    var streakOnARow: Int = 0
    var maxScore: Int = 42

    override func viewDidLoad() {
        super.viewDidLoad()

        self.navigationItem.rightBarButtonItem = UIBarButtonItem(barButtonSystemItem: .compose, target: self, action: #selector(scoreTapped))

        self.setupUI()
        self.getData()
        self.askQuestion()
    }

    func setupUI() {
        self.navigationItem.largeTitleDisplayMode = .never

        self.button1.layer.borderWidth = 1
        self.button2.layer.borderWidth = 1
        self.button3.layer.borderWidth = 1

        self.button1.layer.cornerRadius = 8
        self.button2.layer.cornerRadius = 8
        self.button3.layer.cornerRadius = 8

        self.button1.clipsToBounds = true
        self.button2.clipsToBounds = true
        self.button3.clipsToBounds = true

        self.button1.layer.borderColor = UIColor.lightGray.cgColor
        self.button2.layer.borderColor = UIColor.lightGray.cgColor
        self.button3.layer.borderColor = UIColor.lightGray.cgColor
    }

    func getData() {
        countries += ["estonia", "france", "germany", "ireland", "italy", "monaco", "nigeria", "poland", "russia", "spain", "uk", "us"]
    }

    func askQuestion(action: UIAlertAction? = nil) {
        self.countries.shuffle()
        self.correctAnswer = Int.random(in: 0...2)

        button1.setImage(UIImage(named: countries[0]), for: .normal)
        button2.setImage(UIImage(named: countries[1]), for: .normal)
        button3.setImage(UIImage(named: countries[2]), for: .normal)

        title = self.countries[correctAnswer].uppercased() + " - Pregunta: " + "\(self.currentQuestions + 1)/\(self.totalQuestions)"
    }

    @objc func scoreTapped() {
        let alert = UIAlertController(title: "Puntuación", message: "\(self.score)", preferredStyle: .alert)
        let actionAlert = UIAlertAction(title: "OK", style: .default, handler: nil)
        alert.addAction(actionAlert)
        self.present(alert, animated: true, completion: nil)
    }

    // voy a crear un IBAction y se lo asigno a los 3 botones, utilizando los tags asociados a cada boton
    @IBAction func buttonTapped(_ sender: UIButton) {
        var titleAlert: String?
        var messageAlert: String?

        self.currentQuestions += 1

        if sender.tag == self.correctAnswer {
            self.streakOnARow += 1

            if self.streakOnARow >= 3 {
                self.score += 5
                titleAlert = "ESTÁS EN RACHA! - Puntuación \(self.score)"
                messageAlert = "NO PARES"
            } else {
                self.score += 1
                titleAlert = "Correcto! - Puntuación \(self.score)"
                messageAlert = "Sigue así"
            }
        } else {
            self.streakOnARow = 0

            if self.score != 0 {
                self.score -= 1
            }

            titleAlert = "Error - Puntuación \(self.score)"
            messageAlert = "No desistas, esa bandera era de \(countries[sender.tag].uppercased())"
        }

        if self.currentQuestions == self.totalQuestions {
            titleAlert = "Juego terminado"
            if self.score == self.maxScore {
                messageAlert = "Has conseguido LA MÁXIMA puntuación, \(self.score)"
            } else {
                messageAlert = "Has conseguido una puntuación de \(self.score)"
            }

            self.currentQuestions = 0
            self.score = 0
        }

        let alert = UIAlertController(title: titleAlert, message: messageAlert, preferredStyle: .alert)
        let actionAlert = UIAlertAction(title: "OK", style: .default, handler: askQuestion)
        alert.addAction(actionAlert)
        self.present(alert, animated: true, completion: nil)
    }
}
