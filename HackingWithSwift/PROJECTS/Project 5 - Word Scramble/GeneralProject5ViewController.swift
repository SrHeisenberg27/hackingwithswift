//
//  GeneralProject5ViewController.swift
//  HackingWithSwift
//
//  Created by Álvaro Ferrández Gómez on 24/06/2020.
//  Copyright © 2020 Álvaro Ferrández Gómez. All rights reserved.
//

import UIKit

// Hemos añadido el fichero start.txt

class GeneralProject5ViewController: UITableViewController {

    var allWords = [String]()
    var usedWords = [String]()

    override func viewDidLoad() {
        super.viewDidLoad()

        navigationItem.largeTitleDisplayMode = .never

        navigationItem.rightBarButtonItems = [UIBarButtonItem(barButtonSystemItem: .add, target: self, action: #selector(promptForAnswer)), UIBarButtonItem(barButtonSystemItem: .refresh, target: self, action: #selector(refreshGame))]

        if let startWordsURL = Bundle.main.url(forResource: "words", withExtension: ".txt") {
            if let startWords = try? String(contentsOf: startWordsURL) {
                self.allWords = startWords.components(separatedBy: "\n")
            }
        }

        if allWords.isEmpty {
            self.allWords = ["Vacio"]
        }

        self.startGame()
    }

    func startGame() {
        title = allWords.randomElement()
        usedWords.removeAll(keepingCapacity: true)
        tableView.reloadData()
    }

    @objc func refreshGame() {
        self.startGame()
    }

    @objc func promptForAnswer() {
        let alertController = UIAlertController(title: "Introducir respuesta", message: nil, preferredStyle: .alert)
        alertController.addTextField()

        let submitAction = UIAlertAction(title: "Enviar", style: .default) { [weak self, weak alertController] action in
            guard let answer = alertController?.textFields?[0].text else { return }
            self?.submit(answer)
        }

        alertController.addAction(submitAction)
        present(alertController, animated: true)
    }

    func submit(_ answer: String) {
        let lowerAnswer = answer.lowercased()

        let errorTitle: String
        let errorMessage: String

        if isPossible(word: lowerAnswer) {
            if isOriginal(word: lowerAnswer) {
                if isReal(word: lowerAnswer) {
                    if isMoreThanThree(word: lowerAnswer) {
                        usedWords.insert(answer, at: 0)

                        let indexPath = IndexPath(row: 0, section: 0)
                        tableView.insertRows(at: [indexPath], with: .automatic)

                        return
                    } else {
                        errorTitle = "Palabra muy corta"
                        errorMessage = "Debes introducir más de 3 letras"
                    }
                } else {
                    errorTitle = "Palabra inventada"
                    errorMessage = "No puedes inventar palabras, crack"
                }
            } else {
                errorTitle = "Palabra usada"
                errorMessage = "Debes ser más original..."
            }
        } else {
            guard let title = title?.lowercased() else { return }

            errorTitle = "Imposible"
            errorMessage = "No puedes formar esa palabra a partir de \(title)"
        }

        let alertController = UIAlertController(title: errorTitle, message: errorMessage, preferredStyle: .alert)
        alertController.addAction(UIAlertAction(title: "OK", style: .default))
        present(alertController, animated: true)
    }

    func isPossible(word: String) -> Bool {
        guard var tempWord = title?.lowercased() else { return false }

        for letter in word {
            if let position = tempWord.firstIndex(of: letter) {
                tempWord.remove(at: position)
            } else {
                return false
            }
        }

        return true
    }

    func isOriginal(word: String) -> Bool {
        return !usedWords.contains(word)
    }

    func isReal(word: String) -> Bool {
        let checker = UITextChecker()
        let range = NSRange(location: 0, length: word.utf16.count)
        let misspelledRange = checker.rangeOfMisspelledWord(in: word, range: range, startingAt: 0, wrap: false, language: "es")

        return misspelledRange.location == NSNotFound

        /* es lo mismo que
         
        if misspelledRange.location == NSNotFound {
            return true
        } else {
            return false
        }
         
         */
    }

    func isMoreThanThree(word: String) -> Bool {
        if word.count > 3 {
            return true
        } else {
            return false
        }
    }
}

extension GeneralProject5ViewController {
    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return usedWords.count
    }

    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "WordID", for: indexPath)
        cell.textLabel?.text = usedWords[indexPath.row]
        return cell
    }
}
