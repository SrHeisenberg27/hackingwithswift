//
//  GeneralProject4ViewController.swift
//  HackingWithSwift
//
//  Created by Álvaro Ferrández Gómez on 24/06/2020.
//  Copyright © 2020 Álvaro Ferrández Gómez. All rights reserved.
//

import UIKit
import WebKit

class GeneralProject4ViewController: UIViewController {

    var webView: WKWebView?
    var progressView: UIProgressView?

    var selectedWeb: Int = 0

    var webSites: [String] = ["formula1.com", "f1aldia.com", "soymotor.com", "caranddriver.com/es/formula-1/"]

    override func loadView() {
        webView = WKWebView()
        webView?.navigationDelegate = self
        view = webView
    }

    override func viewDidLoad() {
        super.viewDidLoad()

        navigationItem.largeTitleDisplayMode = .never
        navigationItem.rightBarButtonItem = UIBarButtonItem(title: "Seleccionar página", style: .plain, target: self, action: #selector(buttonTapped))

        let spacer = UIBarButtonItem(barButtonSystemItem: .flexibleSpace, target: nil, action: nil)
        let refresh = UIBarButtonItem(barButtonSystemItem: .refresh, target: webView, action: #selector(webView?.reload))

        progressView = UIProgressView(progressViewStyle: .default)

        if let progressView = self.progressView {
            progressView.sizeToFit()
            let progressButton = UIBarButtonItem(customView: progressView)

            toolbarItems = [progressButton, spacer, refresh]
        }

        navigationController?.isToolbarHidden = false

        if let url = URL(string: "https://" + webSites[0]) {
            self.selectedWeb = 0
            webView?.load(URLRequest(url: url))
            webView?.allowsBackForwardNavigationGestures = true
        }

        webView?.addObserver(self, forKeyPath: #keyPath(WKWebView.estimatedProgress), options: .new, context: nil)
    }

    @objc func buttonTapped() {
        let alertController = UIAlertController(title: "Selecciona la página", message: nil, preferredStyle: .actionSheet)

        if selectedWeb != 0 {
            alertController.addAction(UIAlertAction(title: "F1.com", style: .default, handler: openPage))
        }

        if selectedWeb != 1 {
            alertController.addAction(UIAlertAction(title: "F1 al día", style: .default, handler: openPage))
        }

        if selectedWeb != 2 {
            alertController.addAction(UIAlertAction(title: "Soy Motor", style: .default, handler: openPage))
        }

        if selectedWeb != 3 {
            alertController.addAction(UIAlertAction(title: "Car And Driver", style: .default, handler: openPage))
        }

        alertController.addAction(UIAlertAction(title: "Cancelar", style: .cancel))

        alertController.popoverPresentationController?.barButtonItem = self.navigationItem.rightBarButtonItem

        present(alertController, animated: true)
    }

    func openPage(action: UIAlertAction) {
        guard var actionTitle = action.title else { return }

        switch actionTitle {
        case "F1.com":
            actionTitle = webSites[0]
            self.selectedWeb = 0
        case "F1 al día":
            actionTitle = webSites[1]
            self.selectedWeb = 1
        case "Soy Motor":
            actionTitle = webSites[2]
            self.selectedWeb = 2
        case "Car And Driver":
            actionTitle = webSites[3]
            self.selectedWeb = 3
        default:
            break
        }

        if let url = URL(string: "https://" + actionTitle) {
            webView?.load(URLRequest(url: url))
        }
    }

    func initialPage(action: UIAlertAction) {
        if let url = URL(string: webSites[0]) {
            webView?.load(URLRequest(url: url))
        }
    }
}

extension GeneralProject4ViewController: WKNavigationDelegate {
    func webView(_ webView: WKWebView, didFinish navigation: WKNavigation!) {
        title = webView.title
    }

    override func observeValue(forKeyPath keyPath: String?, of object: Any?, change: [NSKeyValueChangeKey: Any]?, context: UnsafeMutableRawPointer?) {
        if keyPath == "estimatedProgress" {
            progressView?.progress = Float(webView?.estimatedProgress ?? Double(0))
        }
    }

    func webView(_ webView: WKWebView, decidePolicyFor navigationAction: WKNavigationAction, decisionHandler: @escaping (WKNavigationActionPolicy) -> Void) {
        let url = navigationAction.request.url

        if let host = url?.host {
            for website in webSites {
                if host.contains(website) {
                    decisionHandler(.allow)
                    return
                }
            }
        }

        decisionHandler(.cancel)
    }
}
