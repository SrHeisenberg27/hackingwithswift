//
//  GeneralProject6ViewController.swift
//  HackingWithSwift
//
//  Created by Álvaro Ferrández Gómez on 24/06/2020.
//  Copyright © 2020 Álvaro Ferrández Gómez. All rights reserved.
//

import UIKit

class GeneralProject6ViewController: UIViewController {

    @IBOutlet weak var autoLayoutButton: UIButton!
    @IBOutlet weak var programmaticallyButton: UIButton!

    override func viewDidLoad() {
        super.viewDidLoad()

        self.autoLayoutButton.layer.borderWidth = 1
        self.programmaticallyButton.layer.borderWidth = 1

        self.autoLayoutButton.layer.cornerRadius = 8
        self.programmaticallyButton.layer.cornerRadius = 8

        self.autoLayoutButton.clipsToBounds = true
        self.programmaticallyButton.clipsToBounds = true

        self.autoLayoutButton.layer.borderColor = UIColor.lightGray.cgColor
        self.programmaticallyButton.layer.borderColor = UIColor.lightGray.cgColor
    }

    @IBAction func buttonPressed(_ sender: UIButton) {
        var storyboardString = ""
        var viewControllerString = ""

        if sender.tag == 0 {
            storyboardString = "Project6aAutoLayout"
            viewControllerString = "Project6aAutoLayout"
        } else {
            storyboardString = "Project6bProgrammatically"
            viewControllerString = "Project6bProgrammatically"
        }

        let vc = UIStoryboard(name: storyboardString, bundle: nil).instantiateViewController(withIdentifier: viewControllerString)
        self.navigationController?.pushViewController(vc, animated: true)
    }
}
