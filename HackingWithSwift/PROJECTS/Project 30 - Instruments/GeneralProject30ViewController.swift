//
//  GeneralProject30ViewController.swift
//  HackingWithSwift
//
//  Created by Álvaro Ferrández Gómez on 24/06/2020.
//  Copyright © 2020 Álvaro Ferrández Gómez. All rights reserved.
//

import UIKit

class GeneralProject30ViewController: UIViewController {

    override func viewDidLoad() {
        super.viewDidLoad()

        self.view.backgroundColor = .red
    }
}
